<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/about', 'HomeController@about')->name('about');
Route::get('/contact', 'HomeController@contact')->name('contact');
Route::get("/members","HomeController@members");
Route::get("/videos", "VideoController@index");
Route::get("/video/{id}", "VideoController@show");
Route::get("search", "SearchController@search");
Route::post("vote", "ThreadController@vote");

Route::get("/create/thread", "ThreadController@create");
Route::post("/create/thread", "ThreadController@store");
Route::get("/edit/thread/{thread}", "ThreadController@edit");
Route::post("/edit/thread/{thread}", "ThreadController@update");

Route::get("/create/post", "PostController@create");
Route::post("/create/post", "PostController@store");
Route::get("/edit/post/{post}", "PostController@edit");
Route::post("/edit/post/{post}", "PostController@update");

Route::post("create/comment/{post}", 'PostController@addComment');
Route::post("create/answer/{thread}", 'ThreadController@addAnswer');
Route::post("accept/{answer}", 'ThreadController@acceptAnswer');
Route::post("reject/{answer}", 'ThreadController@rejectAnswer');
Route::post("create/reply/{answer}", 'ThreadController@addReply');

Route::get('/edit/{table}/{id}','Admin\ModelController@edit');
Route::post('/edit/{table}/{id}','Admin\ModelController@update');
Route::get('/create/{table}','Admin\ModelController@create');
Route::post('/create/{table}','Admin\ModelController@store');
Route::get('/post/{id}/{slug?}','PostController@show');
Route::get('/posts/{category?}','PostController@index');
Route::get('/threads/{channel?}','ThreadController@index');
Route::get('/thread/{id}/{slug?}','ThreadController@show');

