@extends("layouts.app")

@section("content")
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <h2 class="text-center text-primary">
                    About CLUSTER
                </h2>
                <hr class="primary">

                <div class="post-body">
                    Dolore cupiditate dicta et ut officiis. Cumque minima dolorum et et quia incidunt sunt. In enim
                    dolor illo repellendus excepturi exercitationem. A accusantium commodi culpa placeat laborum.<br>
                    <br>
                    Qui occaecati aut reprehenderit officia non asperiores harum. Aut quia maxime placeat alias. Dolor
                    adipisci est qui.<br> <br>
                    Est quasi doloribus vel reprehenderit quaerat quia expedita. Et qui est quasi dignissimos odio
                    aliquam dolorem. Voluptates debitis qui vitae facere illo eius ut.<br> <br>
                    Veniam in ut enim quia repudiandae ad. Et ad molestias nihil sunt. Dolorum quia dolorem omnis quo
                    sint eveniet quisquam.<br> <br>
                    Est harum incidunt eos fugiat laudantium. Aut et labore deleniti. Qui eum corporis quia consequatur
                    fugit minus adipisci qui. Tempore perferendis dicta dicta reprehenderit et assumenda.
                </div>
                <hr class="primary">

            </div>
        </div>
    </div>
@endsection