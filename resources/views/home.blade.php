@extends('layouts.app')
@section('style')
    <style>
        .carousel .item {
            height: 300px;
        }

        .item img {
            position: absolute;
            top: 0;
            left: 0;
            min-height: 300px;
        }

        .carousel .text-primary {
            text-shadow: 1px 1px 0 whitesmoke,
            1px -1px 0 whitesmoke,
            -1px 1px 0 whitesmoke,
            -1px -1px 0 whitesmoke;

        }
    </style>
@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h3>
                    <strong class="text-primary">CL</strong>ub for
                    <strong class="text-primary">U</strong>pdated
                    <strong class="text-primary">S</strong>earch on
                    Compu<strong class="text-primary">TER</strong></h3>
                <h4>

                    <a href="http://cseku.ac.bd">CSE Discipline</a>,
                    <a href="http://ku.ac.bd">Khulna University</a>

                </h4>
            </div>
            <div class="col-md-4 col-md-offset-2">
                <form method="get" class="" action="{{url("search")}}" style="padding-top: 3rem">
                    <div class="input-group">
                        <input name="search" type="text" class="form-control" placeholder="Search">
                        <div class="input-group-btn">
                            <button class="btn btn-primary" type="submit">
                                <i class="glyphicon glyphicon-search"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
        <div class="row">
            <div id="myCarousel" class="carousel slide" data-ride="carousel" style="margin-top: 2rem;">

                <!-- Indicators -->
                <ol class="carousel-indicators">
                    @foreach($slides as $slide)
                        <li data-target="#myCarousel" data-slide-to="{{$loop->index}}"
                            class="{{$loop->first? 'active': ''}}"></li>
                    @endforeach
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" style="height:35rem;">
                    @foreach($slides as $slide)
                        <div class="item {{$loop->first? 'active': ''}}">
                            <img src="{{asset($slide)}}" alt=""
                                 style="width:100%;">
                            <div class="carousel-caption">
                                {{--<h3 class="text-primary">{{$slide->title}}</h3>--}}
                            </div>
                        </div>
                    @endforeach
                </div>


                <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>

        </div>
        <div class="row" style="padding-top: 3rem">
            <div class="col-md-6" style="text-align: justify">
                <h4 class="text-primary"><strong>History</strong></h4>
                <p class="well">{{config("cluster.history")}}</p>
            </div>
            <div class="col-md-5 col-md-offset-1">
                <h4 class="text-primary"><strong>Latest Posts From Our Blog</strong></h4>
                <ul class="list-group">
                    @forelse($posts as $post)
                        <li class="list-group-item">
                            <a href="{{url("/post/$post->id")}}">

                                <div class="text-container text-justify">
                                    <h4 class="strong-primary">{{$post->title}}</h4>
                                    @if(empty($post->excerpt))
                                        {{substr(strip_tags($post->body), 0, 50)}}...
                                    @else
                                        {!! $post->excerpt !!}
                                    @endif
                                </div>

                            </a>
                        </li>
                    @empty
                        No post available
                    @endforelse
                    @if($posts->count())
                            <li class="list-group-item" >
                                <button class="btn btn-primary" >
                                    <a href="{{url('posts')}}" style="color: whitesmoke">See All Posts</a>
                                </button>
                            </li>
                    @endif
                </ul>
                

            </div>
        </div>
    </div>
@endsection
