@extends('layouts.app')

@section("style")
    <style>
        .post-preview {
            width: 100%;
        }
    </style>
@endsection

@section('content')
    <div class="container">
        <h2 class="text-primary"><strong>CLUSTERed Blog</strong></h2>
        <hr class="primary">
        @php
            $monthList = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",]
        @endphp
        @if ($alert)
            <div class="alert alert-info lead">
                Showing posts
                @if (isset($year) || isset($month))
                    from
                    <strong>
                        @if (isset($month))
                            {{$monthList[$month - 1] . (isset($year)? ' -': '')}}
                        @endif
                        @if (isset($year))
                            {{$year . (isset($category)? ',': '')}}
                        @endif
                    </strong>
                @endif {{-- IF year || month --}}
                @if (isset($category))
                    published in <strong>{{$category}}</strong>
                @endif {{-- IF category --}}
            </div>
        @endif

        <div class="row">
            <div class="col-md-8">
                @forelse($posts as $post)
                    @php
                        $imageExists = Storage::exists("public/posts/$post->id");
                        if ($imageExists){
                            $post->image = Storage::files("public/posts/$post->id")[0];
                        } else {
                            $post->image = null;
                        }
                    @endphp
                    <a href="{{url("/post/$post->id")}}">
                        <div class="post-preview">
                            @if(!empty($post->img))
                                <div class="image-container">
                                    <img src="{{$post->img}}" alt="{{$post->title}}"
                                         class="img img-responsive">
                                </div>
                            @endif
                            <div class="text-container">
                                <h4 class="strong-primary">{{$post->title}}</h4>
                                @if(empty($post->excerpt))
                                    {{substr(strip_tags($post->body), 0, 50)}}...
                                @else
                                    {!! $post->excerpt !!}
                                @endif
                            </div>
                        </div>
                    </a>
                @empty
                    No post available
                @endforelse

                {{$posts->links()}}

            </div>
            <div class="col-md-4">
                @if(Auth::check())
                    <div class="post-info col-md-12">
                        <a class="btn btn-primary" href="{{url("/create/post")}}">Create New</a>
                    </div>
                @endif

                <div class="post-info col-md-12">
                    <h3>Categories</h3>
                    <ul>
                        @foreach($categories as $category)
                            <li>
                                <a href="{{url("posts/$category->category")}}">
                                    {{$category->category}}
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>

                <div class="post-info col-md-12">

                    <h3>Archive</h3>
                    <ul style="font-size: medium">
                        @foreach($hierarchy as $year => $months)
                            <li>
                                <a href="?year={{$year}}">{{$year}} ({{$months->data}})</a>
                                <br>
                                <ul>
                                    @foreach($months as $month)
                                        <li>
                                            <a href="?year={{$year}}&month={{$month->month}}">
                                                {{$monthList[$month->month - 1]}} ({{$month->data}})
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </li>
                        @endforeach
                    </ul>

                </div>

            </div>
        </div>
    </div>
@endsection

@section("script")
    <script>
        const url = window.location.search.slice(1);
        const param = new URLSearchParams(url);
        param.delete('page');
        $(".pagination a").on('click', function () {
            $(this).attr('href', $(this).attr('href') + "&" + param.toString());
        });
    </script>
@endsection