@extends('layouts.app')

@section('content')
    <div class="container" xmlns:v-on="http://www.w3.org/1999/xhtml">
        <article class="row">
            <div class="col-md-6 col-md-offset-1">
                <h2 class="text-center" style="margin-top: 25px;"> {{$post->title}}</h2>
                @if($post->image)
                    <img src="{{asset("storage/app/public/posts/$post->id")}}" alt="{{$post->title}}"
                         class="img img-responsive img-thumbnail">
                @endif
                <div class="post-body">{!! nl2br($post->body) !!}</div>
            </div>

            <div class="post-sidebar col-md-4">
                @if($isOwnPost)
                    <div class="post-info">
                        <a class="btn btn-primary" href="{{url("edit/post/$post->id")}}">Edit Post</a>
                    </div>
                @endif
                <div class="post-info">
                    <span class="strong-primary">Author:</span>
                    {{$post->author->name}}
                </div>
                @if(!empty($post->category))
                    <div class="post-info">
                        <span class="strong-primary">Category:</span>
                        <a href="{{url("posts/{$post->category}")}}">
                            {{$post->category}}
                        </a>
                    </div>
                @endif
                <div class="post-info">
                    <h5 class="strong-primary">Excerpt</h5>
                    <p style="text-align: justify">{!! $post->excerpt !!}</p>
                </div>
                @if(!empty($post->meta_keywords))
                    <div class="post-info">
                        <span class="strong-primary">Keywords:</span>
                        @foreach(explode(",", $post->meta_keywords) as $keyword)
                            <span class="keyword">{{$keyword}}</span>
                        @endforeach
                    </div>
                @endif
                @if(!empty($post->updated_at))
                    <div class="post-info">
                        Published
                        <strong>{{$post->updated_at->diffForHumans()}}</strong>
                    </div>
                @endif

            </div>

        </article>


        <section class="row" id="vm" v-cloak>
            <div class="col-md-8 col-md-offset-2">
                <h2 class="text-primary">Comments</h2>
                <hr class="primary">

                <div class="comment" v-for="comment in comments">
                    <div class="image-container">
                        <img :src="comment.user.image" :alt="comment.user.name"
                             class="img img-responsive">
                    </div>
                    <div class="text-container">
                        <strong>@{{ `${comment.user.first_name} ${comment.user.last_name}` }}</strong>
                        <span v-html="comment.comment"></span>
                    </div>
                </div>

            </div>

            @if(Auth::check())
                <div class="col-md-8 col-md-offset-2">
                    <h3>Add Comment</h3>
                    <div class="form-group">
                        <label for="comment" class="sr-only">
                            Comment
                        </label>

                        <textarea name="comment" id="comment" class="form-control"></textarea>

                    </div>
                    <div class="form-group">

                        <button type="button" class="btn bg-primary" v-on:click="addComment">Comment</button>

                    </div>
                </div>
            @endif
        </section>

    </div>
@endsection

@section("script")

    <script>
        new Vue({
            el: '#vm',
            data: {
                comments: {!! $post->comments !!}
            },
            methods: {
                addComment: function () {
                    const data = {comment: tinyMCE.activeEditor.getContent()}
                    axios.post('{{url("create/comment/$post->id")}}', data)
                        .then(response => {
                            this.comments.splice(this.comments.length, 0, {
                                    comment: data.comment,
                                    user: {
                                        name: '{{$post->author}}',
                                        image: '{{$post->author->image}}'
                                    }
                                }
                            )
                        })
                }
            }
        })
    </script>

    <script src="/js/tinymce/tinymce.min.js"></script>

    <script>
        tinymce.init({
            selector: 'textarea',
            height: 200,
            plugins: [
                'advlist autolink lists link image charmap print preview anchor textcolor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table contextmenu paste code help wordcount'
            ],
            toolbar: 'undo redo |  formatselect | bold italic underline backcolor | alignleft aligncenter alignright alignjustify | bullist numlist | outdent indent | removeformat | help',
            content_css: [
                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                '//www.tinymce.com/css/codepen.min.css'
            ],
            setup: function (editor) {
                editor.on('load', function () {
                    $('.mce-branding').remove();
                })
            }
        });
    </script>
@endsection
