@extends("layouts.app")

@section("content")
    <div class="container col-md-10 col-md-offset-1">
        <h3 class="text-center">
            @if($isCreation)
                Create
            @else
                Update
            @endif
            Post
        </h3>
        <hr class="primary">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form class="form-horizontal" action="{{url($url)}}" method="post">
            {{csrf_field()}}

            {{--Title--}}
            @include("admin.partials.textbox", $title)

            {{--Channel--}}
            @include("admin.partials.select", $associatedCategories)

            {{--Channel--}}
            @include("admin.partials.select", $existingStatuses)

            {{--Excerpt--}}
            @include("admin.partials.textarea", $excerpt)

            {{--Body--}}
            @include("admin.partials.textarea", $body)

            <div class="form-group">

                <div class="col-md-10 col-md-offset-2">
                    <button type="submit" class="btn btn-primary">
                        @if($isCreation)
                            Create
                        @else
                            Update
                        @endif
                    </button>
                </div>
            </div>
        </form>
    </div>
@endsection

@section("script")
    <script src="/js/tinymce/tinymce.min.js"></script>
    <script>
        const baseConfig = {
            toolbar: 'undo redo |  formatselect | bold italic underline backcolor | alignleft aligncenter alignright alignjustify | bullist numlist | outdent indent | removeformat | help',
            content_css: [
                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                '//www.tinymce.com/css/codepen.min.css'
            ],
            setup: function (editor) {
                editor.on('load', function () {
                    $('.mce-branding').remove();
                })
            }
        };
        const pluginConfig = {
            plugins: [
                'advlist autolink lists link image charmap print preview anchor textcolor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table contextmenu paste code help wordcount'
            ],
        };

        tinymce.init(Object.assign({selector: 'textarea[name="excerpt"]', height: 100, menubar:false, statusbar: false,}, baseConfig));
        tinymce.init(Object.assign({selector: 'textarea[name="body"]', height: 200}, baseConfig, pluginConfig));
    </script>
@endsection