@extends("layouts.app")

@section("content")
    <div class="container col-md-10 col-md-offset-1">
        <h3 class="text-center">Edit {{ucfirst($table)}}</h3>
        <hr class="primary">
        <form class="form-horizontal" action="{{$_SERVER['REQUEST_URI']}}" method="post">
            {{csrf_field()}}
            @foreach($keyValuePairs as $key => $value)
                @php $data = ["key" => $key, "value" => $value["value"]] @endphp
                @switch($value["type"])
                    @case("varchar")
                        @php $data["type"] = "text" @endphp
                        @include("admin.partials.textbox", $data)
                        @break
                    @case("text")
                        @include("admin.partials.textarea", $data)
                        @break
                    @case("int")
                        @php $data["type"] = "number" @endphp
                        @include("admin.partials.textbox", $data)
                        @break
                    @case("image")
                        @include("admin.partials.image-upload", $data)
                        @break
                    @case("belongsTo")
                        @php
                            $data["multi"] = false;
                            $data["options"] = $value["options"];
                            $data["values"] = $value["value"]->map(function ($item, $key) {
                                /** @var \Illuminate\Database\Eloquent\Model $item */
                                return $item->getKey();
                            });
                            $data["name"] = $value["name"];
                        @endphp
                        @include("admin.partials.select", $data)
                    @default
                        @break

                @endswitch
            @endforeach

                <div class="form-group">

                    <div class="col-md-10 col-md-offset-2">
                        <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                </div>
        </form>
    </div>
@endsection

@section("script")
    <script src="/js/tinymce/tinymce.min.js"></script>
    <script>
        tinymce.init({
            selector: 'textarea',
            height: 200,
            plugins: [
                'advlist autolink lists link image charmap print preview anchor textcolor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table contextmenu paste code help wordcount'
            ],
            toolbar: 'undo redo |  formatselect | bold italic underline backcolor | alignleft aligncenter alignright alignjustify | bullist numlist | outdent indent | removeformat | help',
            content_css: [
                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                '//www.tinymce.com/css/codepen.min.css'
            ],
            setup: function (editor) {
                editor.on('load', function () {
                    $('.mce-branding').remove();
                })
            }
        });
    </script>
@endsection