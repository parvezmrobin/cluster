<div class="form-group">
    <label for="{{$key}}" class="col-md-2 control-label">{{title_case(snake_case($name, ' '))}}</label>
    <div class="col-md-6 col-lg-4">
        <select name="{{$name}}" id="{{$key}}" class="form-control" {{$multi? 'multiple': ''}}>
            @foreach($options as $option)
                @php($id = is_object($option)? $option->getKey() : $option)
                <option value="{{$id}}"
                        {{$values->contains($id)? 'selected': '' }}>
                    {{$option }}
                </option>
            @endforeach
        </select>
    </div>
</div>