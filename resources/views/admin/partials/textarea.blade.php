<div class="form-group">
    <label for="{{$key}}" class="col-md-2 control-label">
        {{title_case(str_replace('_', ' ', $key))}}
    </label>
    <div class="col-md-10">
        <textarea name="{{$key}}" id="{{$key}}" class="form-control"
               >{{old($key)?: $value}}</textarea>
    </div>
</div>