<div class="form-group">
    <label for="{{$key}}" class="col-md-2 control-label">{{title_case(snake_case($key, ' '))}}</label>
    <div class="col-md-6 col-lg-4">
        <input type="file" accept="image/*" name="{{$key}}" id="{{$key}}" class="form-control" value="{{old($key)?: $value}}">
    </div>
</div>