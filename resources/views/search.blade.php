@extends("layouts.app")

@section("style")
    <style>
        .post-preview {
            width: 100%;
        }
        .post-preview .image-container {
            width: 16.67%;
        }

        .post-preview .text-container {
            width: 83.33%;
        }
    </style>
@endsection

@section("content")
    <div class="container">
        <h2 class="text-primary"><strong>CLUSTERed Search Result</strong></h2>
        <hr class="primary">
        <div class="row">
            <section class="col-md-8">
                @forelse($results as $result)
                    @php $url = ($result instanceof \App\Models\Blog\Post) ? "post": "thread" @endphp
                    <a href="{{url("/$url/$result->id")}}">
                        <div class="post-preview">
                            <div class="image-container lead">
                                <span class="label label-primary">{{ucfirst($url)}}</span>
                            </div>
                            <div class="text-container">
                                <h4 class="strong-primary">{{$result->title}}</h4>
                                {{substr(strip_tags($result->body), 0, 200)}}... <br>
                            </div>
                        </div>
                    </a>
                @empty
                    No result available
                @endforelse

            </section>
        </div>
    </div>
@endsection