@extends('layouts.app')

@section("style")
    <style>
        .comment {
            width: 100%;
        }
    </style>
@endsection

@section('content')
    <div class="container" v-cloak id="vm" xmlns:v-bind="http://www.w3.org/1999/xhtml" xmlns:v-on="http://www.w3.org/1999/xhtml">
        <div class="row">
            <div class="col-md-6 col-md-offset-1">
                <h2 class="text-center"> {{$thread->title}}
                    <span class="lead label label-success" v-if="hasAccepted">Solved</span>
                </h2>
            </div>
        </div>
        <section class="row">

            <div class="col-md-6 col-md-offset-1">
                <div class="post-body">{!! nl2br($thread->body) !!}</div>

                @if($thread->tags_array->count() > 0)
                    <hr class="primary">
                    <strong class="text-info">Tags</strong>:
                    @foreach($thread->tags_array as $tag)
                        <span class="label label-primary">{{$tag}}</span>
                    @endforeach
                @endif
            </div>

            <div class="post-sidebar col-md-4">
                @if($isOwnPost)
                    <div class="post-info">
                        <a class="btn btn-primary" href="{{url("edit/thread/$thread->id")}}">Edit Thread</a>
                    </div>
                @else
                    <div class="post-info">
                        <button type="button" class="btn btn-primary" v-on:click="upvote">
                            Upvote
                            <span class="badge">@{{upvotes}}</span>
                        </button>
                        <button type="button" class="btn btn-warning pull-right" v-on:click="downvote">
                            Downvote
                            <span class="badge">@{{downvotes}}</span>
                        </button>
                    </div>
                @endif
                <div class="post-info">
                    <span class="strong-primary">Author:</span>
                    <a href="{{url("user/".$thread->author->id)}}">
                        {{$thread->author->name}}
                    </a>

                </div>
                @if(!empty($thread->channel))
                    <div class="post-info">
                        <span class="strong-primary">Channel:</span>
                        <a href="{{url("threads/{$thread->channel}")}}">
                            {{$thread->channel}}
                        </a>
                    </div>
                @endif

                @if(!empty($thread->updated_at))
                    <div class="post-info">
                        Published
                        <strong>{{$thread->updated_at->diffForHumans()}}</strong>
                    </div>
                @endif


            </div>


        </section>

        <section class="row">
            <div class="col-md-8 col-md-offset-2" v-show="answers.length">
                <h2 class="text-primary">Answers</h2>
                <hr class="primary">

                <div class="comment" v-for="(answer, i) in answers">
                    <div v-bind:class="{'bg-info': answer.accepted}"
                         v-bind:style="{fontWeight: (answer.accepted? 'bold': 'medium')}">
                        <div class="image-container">
                            <a :href="'{{url("user") . "/"}}' + answer.user.id">
                                <img :src="answer.user.image" :alt="answer.user.name" :title="answer.user.name"
                                     class="img img-responsive">
                            </a>
                        </div>
                        <div class="text-container">
                            <div class="col-md-12">
                                <a :href="'{{url("user") . "/"}}' + answer.user.id">
                                    <strong>@{{answer.user.name}}</strong>
                                </a>
                                <span v-html="answer.answer"></span>
                            </div>
                            <div class="col-md-12" v-if="answer.accepted">
                                <button class="btn btn-warning" v-on:click="reject(answer.id, i)">Reject</button>
                            </div>
                            <div class="col-md-12" v-if="isOwnPost && !hasAccepted">
                                <button class="btn btn-success" v-on:click="accept(answer.id, i)">Accept</button>
                            </div>

                        </div>
                    </div>
                    <hr class="" style="border-top-width: 2px">
                    <div class="row">
                        <div class="col-sm-10 col-sm-offset-2">
                            <div class="comment" v-for="reply in answer.replies">
                                <a :href="'{{url("user") . "/"}}' + reply.user.id">
                                    <strong>@{{reply.user.name}}</strong>
                                </a>
                                <span v-html="reply.reply"></span>
                            </div>
                            <div class="form-horizontal">
                                <h3 class="text-left">Reply To This Answer</h3>
                                <div class="form-group">
                                    <label :for="answer.id"></label>
                                    <div class="col-md-12">
                                        <textarea :id="answer.id" class="form-control"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <button class="btn btn-info" v-on:click="addReply(answer.id, i)">
                                            Reply
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>

            <div class="col-md-8 col-md-offset-2">
                <h3>Add Answer</h3>
                <div class="form-group">
                    <label for="answer" class="sr-only">
                        Answer
                    </label>

                    <textarea name="answer" id="answer" class="form-control"></textarea>

                </div>
                <div class="form-group">

                    <button type="button" class="btn bg-primary" v-on:click="addAnswer">Answer</button>

                </div>
            </div>
        </section>
    </div>
@endsection

@section("script")
    <script>
        new Vue({
            el: '#vm',
            data: {
                answers: {!! $thread->answers !!},
                upvotes: {{$thread->upvotes->count()}},
                downvotes: {{$thread->downvotes->count()}},
                isLoggedIn: {!! $loggedUser? 1: 0 !!},
                isOwnPost: {!! $isOwnPost? 1: 0 !!},
                hasAccepted: {!! $hasAccepted? 1: 0 !!}
            },
            methods: {
                addAnswer: function () {
                    const data = {answer: tinyMCE.activeEditor.getContent()};
                    axios.post('{{url("create/answer/$thread->id")}}', data)
                        .then(response => {
                            this.answers.splice(this.answers.length, 0, {
                                answer: data.answer,
                                user: {
                                    name: '{{$loggedUser}}',
                                    image: '{{optional($loggedUser)->image}}'
                                }
                                }
                            )
                        })
                },
                accept: function(answer_id, i) {
                    axios.post("{{url("accept")}}/" + answer_id)
                        .then(response => {
                            if (response.data.accepted) {
                                this.hasAccepted = true;

                                const ans = this.answers[i];
                                ans.accepted = true;
                            }
                        })
                },
                reject: function(answer_id, i) {
                    axios.post("{{url("reject")}}/" + answer_id)
                        .then(response => {
                            if (response.data.rejected) {
                                this.hasAccepted = false;

                                const ans = this.answers[i];
                                ans.accepted = false;
                            }
                        })
                },
                addReply: function (answer_id, i) {
                    const data = {
                        reply: $('#' + answer_id).val()
                    };
                    axios.post("{{url("create/reply")}}/" + answer_id, data)
                        .then(response => {
                            const ans = this.answers[i];
                            ans.replies.splice(ans.replies.length, 0, {
                                reply: data.reply,
                                id: response.data.id,
                                user: {
                                    name: '{{$loggedUser}}',
                                    image: '{{optional($loggedUser)->image}}'
                                }
                            })
                        })
                },
                upvote: function () {
                    this.vote(true)
                },
                downvote: function () {
                    this.vote(false)
                },
                vote: function (is_upvote) {
                    const data = {
                        is_upvote,
                        thread_id: '{{$thread->id}}'
                    };
                    axios.post("{{url("vote")}}", data)
                        .then(response => {
                            if (response.data.created) {
                                if (data.is_upvote) {
                                    this.upvotes++;
                                } else {
                                    this.downvotes++;
                                }
                            } else {
                                if (data.is_upvote) {
                                    window.alert("You have already upvoted.");
                                } else {
                                    window.alert("You have already downvoted.");
                                }
                            }
                        })
                }
            }
        })
    </script>

    <script src="{{asset("/js/tinymce/tinymce.min.js")}}"></script>

    <script>
        tinymce.init({
            selector: '#answer',
            height: 200,
            plugins: [
                'advlist autolink lists link image charmap print preview anchor textcolor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table contextmenu paste code help wordcount'
            ],
            toolbar: 'undo redo |  formatselect | bold italic underline backcolor | alignleft aligncenter alignright alignjustify | bullist numlist | outdent indent | removeformat | help',
            content_css: [
                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                '//www.tinymce.com/css/codepen.min.css'
            ],
            setup: function (editor) {
                editor.on('load', function () {
                    $('.mce-branding').remove();
                })
            }
        });
    </script>
@endsection
