@extends("layouts.app")

@section("content")
    <div class="container col-md-10 col-md-offset-1">
        <h3 class="text-center">
            @if($isCreation)
                Create
            @else
                Update
            @endif
            Thread
        </h3>
        <hr class="primary">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form class="form-horizontal" action="{{url($url)}}" method="post">
            {{csrf_field()}}

            {{--Title--}}
            @include("admin.partials.textbox", $title)

            {{--Channel--}}
            @include("admin.partials.select", $associatedChannels)

            {{--Body--}}
            @include("admin.partials.textarea", $body)

            {{--Tags--}}
            @include("admin.partials.textbox", $existingTags)

            <div class="form-group">

                <div class="col-md-10 col-md-offset-2">
                    <button type="submit" class="btn btn-primary">
                        @if($isCreation)
                            Create
                        @else
                            Update
                        @endif
                    </button>
                </div>
            </div>
        </form>
    </div>
@endsection

@section("script")
    <script src="/js/tinymce/tinymce.min.js"></script>
    <script>
        tinymce.init({
            selector: 'textarea',
            height: 200,
            plugins: [
                'advlist autolink lists link image charmap print preview anchor textcolor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table contextmenu paste code help wordcount'
            ],
            toolbar: 'undo redo |  formatselect | bold italic underline backcolor | alignleft aligncenter alignright alignjustify | bullist numlist | outdent indent | removeformat | help',
            content_css: [
                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                '//www.tinymce.com/css/codepen.min.css'
            ],
            setup: function (editor) {
                editor.on('load', function () {
                    $('.mce-branding').remove();
                })
            }
        });
    </script>

    <script>
        const tags = [
                @foreach ($tags as $tag)
            {
                tag: "{{$tag}}"
            },
            @endforeach
        ];

        $(document).ready(function () {
            $('#tags').selectize({
                delimiter: ',',
                persist: false,
                valueField: 'tag',
                labelField: 'tag',
                searchField: 'tag',
                options: tags,
                create: function (input) {
                    return {
                        tag: input
                    }
                }
            });
        });
    </script>
@endsection