@extends('layouts.app')

@section("style")
    <style>
        .post-preview {
            width: 100%;
        }
    </style>
@endsection

@section("style")
    <style>
        .post-preview {
            padding: 1.5rem;
        }

        a:hover{
            text-decoration: none; !important;
        }

        .label {
            font-size: small;
        }
    </style>
@endsection

@section('content')
    <div class="container">
        <h2 class="text-primary"><strong>CLUSTERed Forum</strong></h2>
        <hr class="primary">
        @if ($alert)
            <div class="alert alert-info lead">
                Showing threads
                @if (isset($channel))
                    published in <strong>{{$channel}}</strong>
                @endif {{-- IF channel --}}
            </div>
        @endif
        <div class="row">
            <section class="col-md-8">
                @forelse($threads as $thread)
                    <a href="{{url("/thread/$thread->id")}}">
                        <div class="post-preview">
                            <div class="text-container">
                                <h4 class="strong-primary">{{$thread->title}}</h4>
                                {{substr(strip_tags($thread->body), 0, 200)}}... <br>
                            </div>
                            <div class="image-container lead">
                                <div>
                                    @if($thread->answers_count)
                                        <span class="label label-primary">
                                            {{$thread->answers_count}} answers
                                        </span>
                                    @else
                                        <span class="label label-default">
                                            unanswered
                                        </span>
                                    @endif</div>

                                <div style="margin-top: 1rem;">
                                    @if($thread->accepted_answer)
                                        <span class="label label-success">solved</span>
                                    @else
                                        <span class="label label-warning">unsolved</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </a>
                @empty
                    No thread available
                @endforelse

                {{$threads->links()}}

            </section>
            <section class="col-md-4">
                @if(Auth::check())
                    <div class="post-info col-md-12">
                        <a class="btn btn-primary" href="{{url("/create/thread")}}">Create New</a>
                    </div>
                @endif

                <div class="post-info col-md-12">
                    <h3>Channel</h3>
                    <ul>
                        @foreach($channels as $channel)
                            <li>
                                <a href="{{url("threads/$channel->channel")}}">
                                    {{$channel->channel}}
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>

                <div class="post-info col-md-12">
                    <h3>Tags</h3>

                    @foreach($tags as $tag)

                        <a href="{{url("threads/tag:$tag")}}">
                            <span class="label label-info">{{$tag}}</span>
                        </a>

                    @endforeach

                </div>

            </section>
        </div>
    </div>
@endsection