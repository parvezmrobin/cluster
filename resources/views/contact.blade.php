@extends("layouts.app")

@section("content")
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <h2 class="text-primary text-center">Contact</h2>
                <hr class="primary">
                <p class="lead text-center">
                    <b>President</b>: 01794272837 <br>
                    <b>General Secretary</b>: 01521207008 <br>
                    <b>Promotion Secretary</b>: 01913846618
                </p>
                <hr class="primary">
            </div>
        </div>

    </div>
@endsection