@extends('layouts.app')

@section('content')
    <div class="container" id="vm" v-cloak>
        <div class="row col-md-8 col-md-offset-2">
            <h2 class="text-primary"><strong>CLUSTERed Theatre</strong></h2>
            <hr class="primary">
        </div>
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                <a v-for="video in videos" :href="'/video/' + video.id.videoId">
                    <div class="post-preview">

                        <div class="image-container">
                            <img :src="video.snippet.thumbnails.medium.url" :alt="video.snippet.title"
                                 class="img img-responsive">
                        </div>

                        <div class="text-container">
                            <h4 class="strong-primary">@{{ video.snippet.title }}</h4>
                            @{{ video.snippet.description }}
                        </div>
                    </div>
                </a>

            </div>
        </div>
    </div>
@endsection

@section("script")
    <script>
        new Vue({
            el: '#vm',
            data: {
                videos: []
            },
            mounted() {
                const url = 'https://www.googleapis.com/youtube/v3/search?key=AIzaSyC0ESz2gMNK5vsuKcNeeAcIU3zEBqY_5G8&' +
                    'channelId=UCS8dUQu4eadIool0ianM0Xg&part=snippet,id&order=date&type=video';
                axios.get(url)
                    .then(response => {
                        console.log(response.data);
                        this.videos = response.data.items
                    })
            }
        })
    </script>
@endsection