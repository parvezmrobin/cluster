@extends('layouts.app')

@section('content')
    <div class="container" id="vm">
        <article class="row">
            <div class="col-md-10 col-md-offset-1" v-if="video">
                <h2 class="text-center"> @{{ video.title }}</h2>
                <hr class="primary">
                <p class="lead text-center">@{{ video.description }}</p>
                <div class="post-body embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item"
                            src="https://www.youtube.com/embed/{{$id}}"
                            allow="autoplay; encrypted-media" allowfullscreen></iframe>
                </div>
            </div>

        </article>
        <div class="post-info col-md-10 col-md-offset-1" style="margin-top: 1rem" v-if="video">
            Published at
            <strong>@{{ video.publishedAt }}</strong>
        </div>
    </div>
@endsection

@section("script")
    <script>
        new Vue({
            el: '#vm',
            data: {
                video: undefined
            },
            mounted() {
                url = 'https://www.googleapis.com/youtube/v3/videos' +
                    '?key=AIzaSyBezq20FEQ48YlxQQZltgWntzJBVPY7G-Q&part=snippet,id&id={{$id}}'
                axios.get(url)
                    .then(r => {
                        console.log(r.data);
                        this.video = r.data.items[0].snippet
                    })
            }
        })
    </script>
@endsection
