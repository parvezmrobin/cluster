@extends('layouts.app')
@section('style')
    <style>
        .card {
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
            transition: 0.3s;
            text-align: center;
            height: 30rem;
            margin-bottom: 3rem;
        }

        .card .header {
            box-shadow: 0 10px 12px 0 rgba(0, 0, 0, 0.2);
            height: 50%;
        }

        .card div{
            padding: 3rem 2rem 4rem 2rem;
        }

        .card:hover {
            box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2);
        }

    </style>
@endsection
@section('content')
    <div class="container">
        <h2 class="text-primary"><strong>Adviser</strong></h2>
        <hr class="primary">
        <div class="row">
            <div class="col-md-3 col-sm-6">
                <div class="card">
                    <div class="header bg-primary text-danger">
                        <h2>Adviser</h2>
                    </div>
                    <div>
                        <h4><b>Dr. Kazi Masudul Alam</b></h4>
                        <p>
                            Associate Professor <br>
                            CSE Discipline <br>
                            Khulna University
                        </p>
                    </div>
                </div>
            </div>
        </div>


        <h2 class="text-primary"><strong>Committee Members</strong></h2>
        <hr class="primary">
        <div class="row">
            <div class="col-md-3 col-sm-6">
                <div class="card">
                    <div class="header bg-primary text-danger">
                        <h2>President</h2>
                    </div>
                    <div>
                        <h4><b>NAZMUS SAKEEF</b></h4>
                        <p>Contact no: 01794272837</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 col-sm-6">
                <div class="card">
                    <div class="header bg-primary text-danger">
                        <h2>Vice President 01</h2>
                    </div>
                    <div>
                        <h4><b>AL AMIN</b></h4>
                        <p>Contact no: 01677259115</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="card">
                    <div class="header bg-primary text-danger">
                        <h2>Vice President 02</h2>
                    </div>
                    <div>
                        <h4><b>SAIMUM ISLAM</b></h4>
                        <p>Contact no: 01763706245</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 col-sm-6">
                <div class="card">
                    <div class="header bg-primary text-danger">
                        <h2>General Secretary</h2>
                    </div>
                    <div>
                        <h4><b>SHAHIDUL ISLAM</b></h4>
                        <p>Contact no: 01521207008</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="card">
                    <div class="header bg-primary text-danger">
                        <h2>Joint Secretary 01</h2>
                    </div>
                    <div>
                        <h4><b>JM ASHFIQUR RAHMAN</b></h4>
                        <p>Contact no: 01521208399</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="card">
                    <div class="header bg-primary text-danger">
                        <h2>Joint Secretary 02</h2>
                    </div>
                    <div>
                        <h4><b>MD. ABDUL LOTIF</b></h4>
                        <p>Contact no: 01717059192</p>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-3 col-sm-6">
                <div class="card">
                    <div class="header bg-primary text-danger">
                        <h2>IT Secretary</h2>
                    </div>
                    <div>
                        <h4><b>PARVEZ MAHBUB</b></h4>
                        <p>Contact no: 01521203675</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="card">
                    <div class="header bg-primary text-danger">
                        <h2>Asst. IT Secretary</h2>
                    </div>
                    <div>
                        <h4><b>EMAMUL HAQUE MANNA</b></h4>
                        <p>Contact no: 01682715574</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="card">
                    <div class="header bg-primary text-danger">
                        <h2>Publication Secretary</h2>
                    </div>
                    <div>
                        <h4><b>NAZ ZARREEN OISHIE</b></h4>
                        <p>Contact no: 01517107082</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="card">
                    <div class="header bg-primary text-danger">
                        <h3>Asst. Publication Secretary</h3>
                    </div>
                    <div>
                        <h4><b>MD. KAMRUL HASAN</b></h4>
                        <p>Contact no: 01764089371</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="card">
                    <div class="header bg-primary text-danger">
                        <h2>Promotion Secretary</h2>
                    </div>
                    <div>
                        <h4><b>SADIA MAHJABIN</b></h4>
                        <p>Contact no: 01913846618</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="card">
                    <div class="header bg-primary text-danger">
                        <h2>Asst. Promotion Secretary</h2>
                    </div>
                    <div>
                        <h4><b>MOSTAQ AHMED POLOK</b></h4>
                        <p>Contact no: 01725220007</p>
                    </div>
                </div>
            </div>
            

        </div>
        
        <div class="row">
            <div class="col-md-3 col-sm-6">
                <div class="card">
                    <div class="header bg-primary text-danger">
                        <h2>Treasurer</h2>
                    </div>
                    <div>
                        <h4><b>MAHFUJUL ALAM ANTU</b></h4>
                        <p>Contact no: 01917876660</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="card">
                    <div class="header bg-primary text-danger">
                        <h2>Librarian</h2>
                    </div>
                    <div>
                        <h4><b>MUKIT HASAN PRANTO</b></h4>
                        <p>Contact no: 01980492420</p>
                    </div>
                </div>
            </div>

        </div>

    </div>
@endsection

