Only show the editable models
-----------------------------
In line `88` of `TCG\Voyager\Http\Controllers\VoyagerBaseController`
add the following snippet.

```php
$user = Auth::user();
$dataTypeContent = $dataTypeContent->filter(function ($post) use ($user){
     return $user->can('edit', $post);
});
```