<?php

use App\Models\Blog\Post;
use App\Models\Forum\Thread;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

/** @var Factory $factory */
$factory->define(App\Models\User::class, function (Faker $faker) {
    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'email' => $faker->unique()->safeEmail,
        'password' => bcrypt('secret'),
        'remember_token' => str_random(10),
        'role' => 'user',
        'info' => '',
        'bio' => $faker->paragraph(2),
    ];
});

$factory->define(Post::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence,
        'author_id' => 1,
        'category' => null,
        'excerpt' => $faker->paragraph,
        'body' => $faker->paragraphs(5, true),
        'status' => 'PUBLISHED',
    ];
});

$factory->define(Thread::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence,
        'author_id' => 1,
        'channel' => null,
        'body' => $faker->paragraphs(5, true),
    ];
});
