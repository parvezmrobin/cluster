<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(PostsTableSeeder::class);
        $this->call(ChannelSeeder::class);
        $this->call(ThreadsTableSeeder::class);
        $this->call(AnswerSeeder::class);
        $this->call(ReplySeeder::class);
        $this->call(CommentSeeder::class);
        $this->call(VoteSeeder::class);

        $this->call(CarouselSeeder::class);
        $this->call(ImageSeeder::class);
    }
}
