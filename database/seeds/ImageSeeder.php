<?php

use App\Models\Blog\Post;
use Illuminate\Database\Seeder;

class ImageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $posts = Post::all("id");
        $relativePath = DIRECTORY_SEPARATOR . 'public' .
            DIRECTORY_SEPARATOR . 'posts' . DIRECTORY_SEPARATOR;
        $basePath = storage_path('app' . $relativePath);
        foreach ($posts as $post) {
            Storage::makeDirectory($relativePath . $post->id);
            $faker->image($basePath . $post->id);
        }
    }
}
