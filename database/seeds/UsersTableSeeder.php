<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     *
     * @return void
     */
    public function run(){
        factory(User::class, 2)->create();

        factory(User::class)->create([
            'first_name' => 'Admin',
            'last_name' => '',
            'email' => 'admin@admin.com',
            'role' => 'admin',
        ]);

        factory(User::class)->create([
            'first_name' => 'User',
            'last_name' => '',
            'email' => 'user@user.com',
        ]);
    }
}
