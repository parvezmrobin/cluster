<?php

use App\Models\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     */
    public function run(){
        foreach (['admin', 'user'] as $role) {
            Role::insert([
                'role' => $role,
            ]);
        }
    }
}
