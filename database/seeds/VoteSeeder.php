<?php

use App\Models\Forum\Thread;
use App\Models\Forum\Vote;
use App\Models\User;
use Illuminate\Database\Seeder;

class VoteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $threads = Thread::all('id');
        $users = User::all('id');
        foreach ($threads as $thread) {
            for ($i = 0; $i < 4; $i++) {
                $vote = new Vote();
                $vote->thread_id = $thread->id;
                $vote->user_id = $users[$i]->id;
                $vote->is_upvote = rand(0, 1);
                $vote->save();
            }
        }
    }
}
