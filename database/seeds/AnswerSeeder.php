<?php

use App\Models\Forum\Answer;
use App\Models\Forum\Thread;
use Illuminate\Database\Seeder;

class AnswerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $userIds = \App\Models\User::all('id')->map(function ($user) {
            return $user->id;
        })->all();

        $threadIds = Thread::all('id')->map(function ($thread) {
            return $thread->id;
        })->all();

        for($i = 0; $i < 500; $i++) {
            $ans = new Answer([
                "user_id" => array_random($userIds),
                "thread_id" => array_random($threadIds),
                "answer" => $faker->paragraph(2),
            ]);
            $ans->save();
        }
    }
}
