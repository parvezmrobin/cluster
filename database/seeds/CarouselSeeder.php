<?php

use Illuminate\Database\Seeder;

class CarouselSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $relativePath = DIRECTORY_SEPARATOR . 'public' .
            DIRECTORY_SEPARATOR . 'carousel' . DIRECTORY_SEPARATOR;
        $basePath = storage_path('app' . $relativePath);
        Storage::makeDirectory($relativePath);
        for ($i = 0; $i < 10; $i++) {
            $faker->image($basePath, 1280, 480, 'abstract');
        }
    }
}
