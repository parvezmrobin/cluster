<?php

use App\Models\Forum\Thread;
use Illuminate\Database\Seeder;

class ThreadsTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     */
    public function run()
    {
        $userIds = \App\Models\User::all('id')->map(function ($user) {
            return $user->id;
        })->all();

        $channels = \App\Models\Forum\Channel::all('channel')->map(function ($channel) {
            return $channel->channel;
        })->all();


        for ($i = 0; $i < 50; $i++) {
            factory(Thread::class)->create([
                'author_id' => array_random($userIds),
                'channel' => array_random($channels),
            ]);
        }
    }
}
