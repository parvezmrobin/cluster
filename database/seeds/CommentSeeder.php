<?php

use App\Models\Blog\Comment;
use App\Models\Blog\Post;
use Illuminate\Database\Seeder;

class CommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $userIds = \App\Models\User::all('id')->map(function ($user) {
            return $user->id;
        })->all();

        $postIds = Post::all('id')->map(function ($post) {
            return $post->id;
        })->all();

        for($i = 0; $i < 200; $i++) {
            $comment = new Comment([
                "user_id" => array_random($userIds),
                "post_id" => array_random($postIds),
                "comment" => $faker->paragraph(2),
            ]);
            $comment->save();
        }
    }

}
