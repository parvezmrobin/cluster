<?php

use App\Models\Forum\Channel;
use Illuminate\Database\Seeder;

/**
 * cluster
 * User: Parvez M Robin
 * Mail: parvezmrobin@gmail.com
 * Date: 7/12/2018
 * Time: 5:07 PM
 */
class ChannelSeeder extends Seeder
{
    public function run(){
        for($i = 1; $i <= 5; $i++) {
            Channel::insert([
                'channel' => "Channel $i",
            ]);
        }
    }
}