<?php

use App\Models\Blog\Post;
use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     */
    public function run()
    {
        $userIds = \App\Models\User::all('id')->map(function ($user) {
            return $user->id;
        })->all();

        $categories = \App\Models\Blog\Category::all('category')->map(function ($category) {
            return $category->category;
        })->all();

        for($i = 0; $i < 50; $i++) {
            factory(Post::class)->create([
                'author_id' => array_random($userIds),
                'category' => array_random($categories),
            ]);
        }
    }
}
