<?php

use App\Models\Blog\Category;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     *
     * @return void
     */
    public function run(){
        for($i = 1; $i <= 5; $i++) {
            Category::insert([
                'category' => "Category $i",
            ]);
        }
    }
}
