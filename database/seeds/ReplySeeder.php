<?php

use App\Models\Forum\Answer;
use App\Models\Forum\Reply;
use Illuminate\Database\Seeder;

class ReplySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $userIds = \App\Models\User::all('id')->map(function ($user) {
            return $user->id;
        })->all();

        $answerIds = Answer::all('id')->map(function ($ans) {
            return $ans->id;
        })->all();

        for($i = 0; $i < 1000; $i++) {
            $post = new Reply([
                "user_id" => array_random($userIds),
                "answer_id" => array_random($answerIds),
                "reply" => $faker->paragraph(2),
            ]);
            $post->save();
        }
    }
}
