<?php

use App\Models\Blog\Category;
use App\Models\Blog\Post;
use App\Models\User;

return [
    "post" => [
        "model" => Post::class,
        "hidden" => ["author_id", "category", "last_editor"],
        "image" => ["image"],
        "relation" => ["category" => "cat"],
        "editor" => "author_id",
    ],
    "thread" => [
        "model" => \App\Models\Forum\Thread::class,
        "relation" => ["channel" => "cnl"],
        "hidden" => ["author_id"],
    ],
    "user" => [
        "model" => User::class,
        "hidden" => [
            "password", "remember_token", "last_editor", "info",
        ],
        "image" => ["avatar"],
        "relation" => ["role" => "user_role"],
    ],
    "category" => [
        "model" => Category::class,
    ]
];