<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Table;
use Auth;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Request;
use RuntimeException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ModelController extends Controller
{

    public function __construct()
    {
        $this->middleware("auth");
    }

    public function create($table)
    {
        list($model, $hidden, $image, $relation, $editor) = $this->getConfiguration($table);
        $columns = $this->getColumn($table);
        info($table, $columns);

        $keyValuePairs = [];
        foreach ($columns as $column) {
            $columnName = $column->COLUMN_NAME;
            if (in_array($columnName, $hidden)) {
                continue;
            }
            if (in_array($columnName, $image)) {
                $column->DATA_TYPE = "image";
            }
            $keyValuePairs[$column->COLUMN_NAME] = [
                "value" => null,
                "type" => $column->DATA_TYPE
            ];
        }

        $firstInstance = $model::first();

        foreach ($relation as $relationColumn => $relationName) {
            /** @var Relation $relationQuery */
            $relationQuery = $firstInstance->$relationName();
            /** @var Model $relatedModel Sample model to get type */
            $relatedModel = $relationQuery->first();
            /** @var string $relatedClass class of the related model */
            $relatedClass = get_class($relatedModel);
            /** @var Collection $allPossibleRelatedModels */
            $allPossibleRelatedModels = $relatedClass::all();

            $keyValuePairs[$relationName] = [
                "value" => collect([]),
                "options" => $allPossibleRelatedModels,
            ];

            if (is_numeric($relationColumn)) {
                $keyValuePairs[$relationName]["name"] = $relationName;
            } else {
                $keyValuePairs[$relationName]["name"] = $relationColumn;
            }

            switch (get_class($relationQuery)) {
                case BelongsTo::class:
                    $keyValuePairs[$relationName]["type"] = "belongsTo";
                    break;
                case BelongsToMany::class:
                    $keyValuePairs[$relationName]["type"] = "belongsToMany";
                    break;
                case HasOne::class:
                    $keyValuePairs[$relationName]["type"] = "hasOne";
                    break;
                case HasMany::class:
                    $keyValuePairs[$relationName]["type"] = "hasMany";
                    break;
                default:
                    break;
            }
        }

        return view("admin.create", compact("keyValuePairs", "table"));
    }

    public function store(Request $request, $table)
    {
        list($model, $hidden, $image, $relation, $editor) = $this->getConfiguration($table);

        $data = $request->all();
        unset($data["_token"]);

        $instance = new $model();

        foreach ($data as $attr => $value) {
            if (in_array($attr, $image)) {
                throw new RuntimeException("Not Implemented.");
            }
            if (in_array($attr, $hidden)) {
                continue;
            }
            $instance->$attr = $value;
        }
        foreach ($editor as $e) {
            $instance->$e = Auth::id();
        }
        $instance->save();

        return response()->redirectTo("$table/$instance->id");
    }

    /**
     * @param $table
     * @param $id
     * @return \Illuminate\View\View
     */
    public function edit($table, $id)
    {
        list($hidden, $image, $relation, $instance, $columns) = $this->getEverything($table, $id);

        $keyValuePairs = [];
        foreach ($columns as $column) {
            $columnName = $column->COLUMN_NAME;
            if (in_array($columnName, $hidden)) {
                continue;
            }
            if (in_array($columnName, $image)) {
                $column->DATA_TYPE = "image";
            }
            $keyValuePairs[$column->COLUMN_NAME] = [
                "value" => $instance->$columnName,
                "type" => $column->DATA_TYPE
            ];
        }

        foreach ($relation as $relationColumn => $relationName) {
            /** @var Relation $relationQuery */
            $relationQuery = $instance->$relationName();
            /** @var Collection $relatedModels */
            $relatedModels = $relationQuery->get();
            $relatedClass = get_class($relatedModels->first());
            /** @var Collection $allPossibleRelatedModels */
            $allPossibleRelatedModels = $relatedClass::all();

            $keyValuePairs[$relationName] = [
                "value" => $relatedModels,
                "options" => $allPossibleRelatedModels,
            ];

            if (is_numeric($relationColumn)) {
                $keyValuePairs[$relationName]["name"] = $relationName;
            } else {
                $keyValuePairs[$relationName]["name"] = $relationColumn;
            }

            switch (get_class($relationQuery)) {
                case BelongsTo::class:
                    $keyValuePairs[$relationName]["type"] = "belongsTo";
                    break;
                case BelongsToMany::class:
                    $keyValuePairs[$relationName]["type"] = "belongsToMany";
                    break;
                case HasOne::class:
                    $keyValuePairs[$relationName]["type"] = "hasOne";
                    break;
                case HasMany::class:
                    $keyValuePairs[$relationName]["type"] = "hasMany";
                    break;
                default:
                    break;
            }
        }

//        dump($keyValuePairs);

        return view("admin.edit", compact("keyValuePairs", "table"));
    }

    public function update(Request $request, $table, $id)
    {
        list($hidden, $image, $relation, $instance) = $this->getConfigAndInstance($table, $id);

        $data = $request->all();
        unset($data["_token"]);

        foreach ($data as $attr => $value) {
            if (in_array($attr, $image)) {
                throw new RuntimeException("Not Implemented.");
            }
            if (in_array($attr, $hidden)) {
                continue;
            }
            $instance->$attr = $value;
        }
        $instance->save();

        return response()->redirectTo("$table/$id");
    }

    /**
     * @param $table
     * @param $id
     * @return array
     */
    public function getEverything($table, $id): array
    {
        list($hidden, $image, $relation, $instance) = $this->getConfigAndInstance($table, $id);

        $columns = $this->getColumn($table);
        return array($hidden, $image, $relation, $instance, $columns);
    }

    /**
     * @param $table
     * @return array
     */
    public function getConfiguration($table): array
    {
        $entry = config("table.$table");
        info($entry);
        if (is_array($entry)) {
            $model = $entry["model"];
            $hidden = array_has($entry, "hidden") ? $entry["hidden"] : [];
            $image = array_has($entry, "image") ? $entry["image"] : [];
            $relation = array_has($entry, "relation") ? $entry["relation"] : [];
            $editor = array_has($entry, "editor") ? $entry["editor"] : [];
        } else {
            $model = $entry;
            $hidden = [];
            $image = [];
            $relation = [];
            $editor = [];
        }
        $editor = (array)$editor;

        if (is_null($model)) {
            throw new NotFoundHttpException();
        }
        return array($model, $hidden, $image, $relation, $editor);
    }

    /**
     * @param $table
     * @param $id
     * @return array
     */
    public function getConfigAndInstance($table, $id): array
    {
        list($model, $hidden, $image, $relation, $editor) = $this->getConfiguration($table);

        /** @var Model $instance */
        $instance = $model::findOrFail($id);
        return array($hidden, $image, $relation, $instance);
    }

    /**
     * @param $table
     * @return mixed
     */
    public function getColumn($table)
    {
        $columns = (new Table)->table(str_plural($table))
            ->where('COLUMN_NAME', '!=', 'id')
            ->get();
        return $columns;
    }
}
