<?php

namespace App\Http\Controllers;

use App\Models\Blog\Post;
use App\Models\Forum\Thread;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function search(Request $request)
    {
        $keyword = $request->get("search");
        $str = "%$keyword%";

        /** @var Collection $posts */
        $posts = Post::where("title", "like", $str)
            ->orWhere("body", "like", $str)
            ->orWhereHas("author", function (Builder $builder) use ($str) {
                $builder->where("first_name", "like", $str)
                    ->orWhere("last_name", "like", $str);
            })
            ->orWhere("category", "like", $str)
            ->get();

        /** @var Collection $threads */
        $threads = Thread::where("title", "like", $str)
            ->orWhere("body", "like", $str)
            ->orWhereHas("author", function (Builder $builder) use ($str) {
                $builder->where("first_name", "like", $str)
                    ->orWhere("last_name", "like", $str);;
            })
            ->orWhere("tags", "like", $str)
            ->orWhere("channel", "like", $str)
            ->get();

        $results = $posts->union($threads)->sortByDesc('updated_at');
        return view("search", compact("results"));
    }
}
