<?php

namespace App\Http\Controllers;

use App\Models\Blog\Category;
use App\Models\Blog\Post;
use App\Models\User;
use Exception;
use HTMLPurifier;
use HTMLPurifier_Config;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PostController extends Controller
{
    /**
     * @param Request $request
     * @param null $category
     * @return View
     * @throws Exception
     */
    public function index(Request $request, $category = null)
    {
        $dbConnection = config('database.default');
        if (array_search($dbConnection, ["pgsql", "mysql"]) === false) {
            abort(500,'Database not supported');
        }

        $data = [];

        /** @var Builder $query */
        $query = Post::query();
        if ($category !== null) {
            $category = Category::where("category", $category)->first();
            if ($category !== null) {
                $query->where("category", $category->category);
                $data["category"] = $category->category;
            }
        }

        if ($request->has('year')) {
            $data["year"] = $request->get('year');
            $yearProjector = ($dbConnection === 'pgsql')? "date_part('YEAR', created_at)" : 'YEAR(created_at)';
            $query->where(DB::raw($yearProjector), $data["year"]);
        }

        if ($request->has('month')) {
            $data["month"] = $request->get('month');
            $yearProjector = ($dbConnection === 'pgsql')? "date_part('MONTH', created_at)" : 'MONTH(created_at)';
            $query->where(DB::raw($yearProjector), $data["month"]);
        }

        $data["posts"] = $query->latest()->paginate();

        // crawl the first image for pre-viewing, if available
        foreach ($data["posts"] as $post) {
            $crawler = new Crawler($post->body);
            $imgNode = $crawler->filter('img')->first()->getNode(0);
            if ($imgNode) {
                $post->img = $imgNode->getAttribute('src');
            } else {
                $post->img = '';
            }

        }

        $data["categories"] = Category::all();

        /** @var Collection $groupByYearSecond */
        if ($dbConnection === 'pgsql') {
            $projection = DB::raw("count(id) AS data, date_part('YEAR', created_at) AS year, date_part('MONTH', created_at) AS month");
        } else {
            $projection = DB::raw('count(id) AS data, YEAR(created_at) AS year, MONTH(created_at) AS month');
        }
        $groupByYearSecond = Post::select($projection)
            ->groupby('year', 'month')
            ->get();
        $hierarchy = $groupByYearSecond->groupBy('year');
        $hierarchy->each(function (Collection $year) {
            $year->data = $year->sum(function ($month) {
                return $month->data;
            });
        });

        $data["hierarchy"] = $hierarchy;
        $data["alert"] = (isset($data["year"]) || isset($data["month"]) || isset($data["category"]));

        return view('post/index', $data);
    }

    public function show($id)
    {
        /** @var Post $post */
        $post = Post::findOrFail($id);
        $exists = Storage::exists("public/users/{$post->author->id}");
        if ($exists) {
            $post->author->image = "/storage/users/" . $post->author->id;
        } else {
            $post->author->image = "/storage/users/default.png";
        }

        $post->load('comments.user');
        foreach ($post->comments as $comment) {
            $exists = Storage::exists("public/users/{$comment->user->id}");
            if ($exists) {
                $comment->user->image = "/storage/users/" . $comment->user->id;
            } else {
                $comment->user->image = "/storage/users/default.png";
            }
        }
        $isOwnPost = false;
        if (Auth::check()) {
            $loggedUser = Auth::user();
            if ($loggedUser->id === $post->author->id or $loggedUser->role == 'admin') {
                $isOwnPost = true;
            }
        }
        return view('post/show', compact("post", "isOwnPost"));
    }

    public function create()
    {
        if(!Auth::check()){
            abort(401);
        }

        $categories = Category::all();
        $title = ["key" => "title", "value" => null, "type" => "text"];
        $excerpt = ["key" => "excerpt", "value" => null];
        $body = ["key" => "body", "value" => null];
        $associatedCategories = [
            "multi" => false,
            "key" => "category",
            "name" => "category",
            "options" => $categories,
            "values" => collect([])
        ];
        $url = "create/post";
        $isCreation = true;

        $statuses = collect(['PUBLISHED', 'DRAFT']);
        $existingStatuses = ["multi" => false, "key" => "status", "name" => "status", "options" => $statuses, "values" => collect(['DRAFT'])];

        $data = compact(
            "title", "body", "excerpt", "associatedCategories",
            "url", "isCreation", "existingStatuses"
        );
        return view("post.create", $data);
    }


    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        if (!Auth::check()) {
            abort(401);
        }

        $this->validate($request, [
            "title" => "required",
            "body" => "required",
            "status" => "required",
            "category" => "required|exists:categories,category",
        ]);

        $data = $request->only(["title", "body", "excerpt", "category", "status"]);
        $htmlPurifierConfig = HTMLPurifier_Config::createDefault();
        $htmlPurifier = new HTMLPurifier($htmlPurifierConfig);
        $data["body"] = $htmlPurifier->purify($data["body"]);
        $data["excerpt"] = $htmlPurifier->purify($data["excerpt"]);
        $data["author_id"] = Auth::user()->id;

        /** @var Post $post */
        $post = Post::create($data);
        $post->save();

        return redirect("/post/$post->id");
    }

    public function edit(Post $post)
    {
        /** @var User $user */
        if (is_null($user = Auth::user())) {
            abort(401);
        }

        if ($post->author_id !== $user->id && !$user->isAdmin()) {
            abort(401);
        }

        $categories = Category::all();
        $title = ["key" => "title", "value" => $post->title, "type" => "text"];
        $excerpt = ["key" => "excerpt", "value" => $post->excerpt];
        $body = ["key" => "body", "value" => $post->body];
        $associatedCategories = [
            "multi" => false,
            "key" => "category",
            "name" => "category",
            "options" => $categories,
            "values" => collect([$post->category])
        ];
        $url = "edit/post/$post->id";
        $isCreation = false;

        $statuses = collect(['PUBLISHED', 'DRAFT']);
        $existingStatuses = [
            "multi" => false,
            "key" => "status",
            "name" => "status",
            "options" => $statuses,
            "values" => collect([$post->status])
        ];

        $data = compact(
            "title", "body", "excerpt", "associatedCategories",
            "url", "isCreation", "existingStatuses"
        );
        return view("post.create", $data);
    }

    public function update(Post $post, Request $request)
    {
        /** @var User $user */
        if (is_null($user = Auth::user())) {
            abort(401);
        }

        if ($post->author_id !== $user->id && !$user->isAdmin()) {
            abort(401);
        }

        $this->validate($request, [
            "title" => "required",
            "body" => "required",
            "status" => "required",
            "category" => "required|exists:categories,category",
        ]);

        $data = $request->only(["title", "body", "excerpt", "category", "status"]);
        $htmlPurifierConfig = HTMLPurifier_Config::createDefault();
        $htmlPurifier = new HTMLPurifier($htmlPurifierConfig);
        $data["body"] = $htmlPurifier->purify($data["body"]);
        $data["excerpt"] = $htmlPurifier->purify($data["excerpt"]);
        $data["author_id"] = $user->id;

        $post->fill($data);
        $post->save();

        return redirect("/post/$post->id");
    }



    public function addComment(Post $post, Request $request)
    {
        if (!($user = Auth::user())) {
            throw new NotFoundHttpException();
        }

        $commentId = $post->comments()->insertGetId([
            "comment" => $request->get("comment"),
            "user_id" => $user->id,
            "post_id" => $post->id,
        ]);

        return $commentId;
    }
}
