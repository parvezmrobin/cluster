<?php

namespace App\Http\Controllers;

use App\Models\Forum\Answer;
use App\Models\Forum\Channel;
use App\Models\Forum\Thread;
use App\Models\Forum\Vote;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ThreadController extends Controller
{
    public function index($channel = null)
    {
        $tag = false;
        /** @var Builder $query */
        $query = Thread::published();
        if ($channel !== null) {
            if (starts_with($channel, "tag:")) {
                $tag = str_after($channel, "tag:");
            } else {
                $channel = Channel::where("channel", $channel)->first();
                if ($channel !== null) {
                    $query->where("channel", $channel->channel);
                }
            }
        }
        if ($tag) {
            $query->where("tags", "like", "%$tag%");
        }
        $threads = $query->withCount([
            'answers',
            'answers as accepted_answer' => function (Builder $query) {
                $query->where('accepted', 1);
            }
        ])->latest('updated_at')->paginate();
        $channels = Channel::all();
        $tags = Thread::existingTags()->pluck('name');
        $alert = !!$tag || !!$channel;

        return view('thread/index', compact('threads', 'channels', "tags", "alert", "channel", "tag"));
    }

    public function show($id)
    {
        /** @var Thread $thread */
        $thread = Thread::findOrFail($id);
        $exists = Storage::exists("public/users/{$thread->author->id}");
        if ($exists) {
            $thread->author->image = "/storage/users/" . $thread->author->id;
        } else {
            $thread->author->image = "/storage/users/default.png";
        }

        $thread->load(['answers.user', 'answers.replies.user']);
        $hasAccepted = $thread->answers->contains(function ($answer) {
            return $answer->accepted;
        });
        foreach ($thread->answers as $answer) {
            $exists = Storage::exists("public/users/{$answer->user->id}");
            if ($exists) {
                $answer->user->image = "/storage/users/" . $answer->user->id;
            } else {
                $answer->user->image = "/storage/users/default.png";
            }

            foreach ($answer->replies as $reply) {
                $exists = Storage::exists("public/users/{$reply->user->id}");
                if ($exists) {
                    $reply->user->image = "/storage/users/" . $reply->user->id;
                } else {
                    $reply->user->image = "/storage/users/default.png";
                }
            }
        }
        $isOwnPost = false;
        $loggedUser = null;
        if (Auth::check()) {
            /** @var User $loggedUser */
            $loggedUser = Auth::user();
            if ($loggedUser->id === $thread->author->id or $loggedUser->role == 'admin') {
                $isOwnPost = true;
            }

            $exists = Storage::exists("public/users/{$loggedUser->id}");
            if ($exists) {
                $loggedUser->image = "/storage/users/" . $loggedUser->id;
            } else {
                $loggedUser->image = "/storage/users/default.png";
            }
        }

        $data = compact("thread", "isOwnPost", "loggedUser", "hasAccepted");
        return view('thread/show', $data);
    }

    public function create()
    {
        if(!Auth::check()){
            abort(401);
        }

        $channels = Channel::all();
        $title = ["key" => "title", "value" => null, "type" => "text"];
        $body = ["key" => "body", "value" => null];
        $associatedChannels = ["multi" => false, "key" => "channel", "name" => "channel", "options" => $channels,
            "values" => collect([])];
        $url = "create/thread";
        $isCreation = true;

        $tags = Thread::existingTags()->pluck('name');
        $existingTags = ["key" => "tags", "value" => null, "type" => "text"];

        $data = compact(
            "title", "body", "associatedChannels",
            "url", "isCreation", "tags", "existingTags"
        );
        return view(
            "thread.create",
            $data
        );
    }

    /**
     * @param Request $request
     * @return RedirectResponse|Redirector
     */
    public function store(Request $request)
    {
        if (!Auth::check()) {
            abort(401);
        }

        $this->validate($request, [
            "title" => "required",
            "body" => "required",
            "channel" => "required|exists:channels,channel",
        ]);

        $data = $request->only(["title", "body", "channel"]);
        $data["author_id"] = Auth::user()->id;
        /** @var Thread $thread */
        $thread = Thread::create($data);
        if ($request->get("tags")) {
            $thread->tag(explode(',', $request->get("tags")));
        }
        $thread->save();

        return redirect("thread/$thread->id");
    }

    public function edit(Thread $thread)
    {
        if (is_null($user = Auth::user())) {
            abort(401);
        }

        if ($thread->author_id !== $user->id && !$user->isAdmin()) {
            abort(401);
        }

        $channels = Channel::all();
        $title = ["key" => "title", "value" => $thread->title, "type" => "text"];
        $body = ["key" => "body", "value" => $thread->body];
        $associatedChannels = ["multi" => false, "key" => "channel", "name" => "channel", "options" => $channels,
            "values" => collect([$thread->channel])];
        $url = "edit/thread/$thread->id";
        $isCreation = false;

        $tags = Thread::existingTags()->pluck('name');
        $existingTags = ["key" => "tags", "value" => $thread->tags, "type" => "text"];

        $data = compact(
            "title", "body", "associatedChannels",
            "url", "isCreation", "tags", "existingTags"
        );
        return view(
            "thread.create",
            $data
        );
    }

    public function update(Thread $thread, Request $request)
    {
        if (is_null($user = Auth::user())) {
            abort(401);
        }

        if ($thread->author_id !== $user->id && !$user->isAdmin()) {
            abort(401);
        }

        $this->validate($request, [
            "title" => "required",
            "body" => "required",
            "channel" => "required|exists:channels,channel",
        ]);

        $data = $request->only(["title", "body", "channel"]);
        $data["author_id"] = Auth::user()->id;
        /** @var Thread $thread */
        $thread = $thread->fill($data);
        if ($request->get("tags")) {
            $thread->tag(explode(',', $request->get("tags")));
        }
        $thread->save();

        return redirect("thread/$thread->id");
    }

    public function addAnswer(Thread $thread, Request $request)
    {
        if (!($user = Auth::user())) {
            throw new NotFoundHttpException();
        }

        $answerId = $thread->answers()->insertGetId([
            "answer" => $request->get("answer"),
            "user_id" => $user->id,
            "thread_id" => $thread->id,
        ]);

        return $answerId;
    }

    public function addReply(Answer $answer, Request $request)
    {
        if (!($user = Auth::user())) {
            throw new NotFoundHttpException();
        }

        $replyId = $answer->replies()->insertGetId([
            "reply" => $request->get("reply"),
            "user_id" => $user->id,
            "answer_id" => $answer->id,
        ]);

        return $replyId;
    }

    public function acceptAnswer(Answer $answer)
    {
        if (!($user = Auth::user())) {
            throw new NotFoundHttpException();
        }
        $thread = $answer->thread;
        if ($user->id !== $thread->author_id) {
            throw new NotFoundHttpException();
        }

        $hasAccepted = !! $thread->answers()->where("accepted", 1)->count();

        if($hasAccepted) {
            return ["accepted" => false];
        }

        $answer->accepted = 1;
        $answer->save();
        return ["accepted" => true];
    }

    public function rejectAnswer(Answer $answer)
    {
        if (!($user = Auth::user())) {
            throw new NotFoundHttpException();
        }
        $thread = $answer->thread;
        if ($user->id !== $thread->author_id) {
            throw new NotFoundHttpException();
        }

        $answer->accepted = 0;
        $answer->save();
        return ["rejected" => true];
    }

    public function vote(Request $request)
    {
        if (!($user = Auth::user())) {
            throw new NotFoundHttpException();
        }

        $this->validate($request, [
            "thread_id" => "required",
            "is_upvote" => "required",
        ]);

        $vote = Vote::where("user_id", $user->id)
            ->where("thread_id", $request->get("thread_id"))
            ->first();
        if ($vote) {
            return ["created" => false, "is_upvote" => $vote->is_upvote];
        }

        $attributes = [
            "user_id" => $user->id,
            "thread_id" => $request->get("thread_id"),
            "is_upvote" => $request->get("is_upvote"),
        ];
        Vote::create($attributes);
        return ["created" => true];
    }
}
