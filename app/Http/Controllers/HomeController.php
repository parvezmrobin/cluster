<?php

namespace App\Http\Controllers;

use App\Models\Blog\Post;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index()
    {
        $posts = Post::orderBy('updated_at', 'desc')
            ->take(2)
            ->get();
        $carouselDir = 'public' . DIRECTORY_SEPARATOR . 'carousel' . DIRECTORY_SEPARATOR;
        $slides = Storage::files($carouselDir);
        $slides = array_map(function ($slide) {
            return str_replace_first("public", "storage", $slide);
        }, $slides);

        return view('home', compact('slides', 'posts'));
    }

    public function members(){
        return view( 'members');
    }

    public function about()
    {
        return view("about");
    }

    public function contact()
    {
        return view("contact");
    }
}
