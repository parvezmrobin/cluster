<?php

namespace App\Models\Blog;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * @property string category
 * @method static Builder where(string $string, $category)
 */
class Category extends Model
{
    protected $primaryKey = 'category';
    protected $keyType = 'string';

    public function __toString()
    {
        return $this->category;
    }
}
