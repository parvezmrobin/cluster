<?php

namespace App\Models\Blog;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

/**
 * @property User user
 */
class Comment extends Model
{
    protected $fillable = ["comment", "user_id"];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
