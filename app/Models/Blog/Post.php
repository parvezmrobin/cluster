<?php

namespace App\Models\Blog;

use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * @property Comment[] comments
 * @property User author
 * @property int id
 * @property string title
 * @property string excerpt
 * @property string body
 * @property Category category
 * @property string status
 * @property string author_id
 * @method static published()
 * @method static findOrFail(int $id)
 * @method static Builder orderBy(string $path, string $order)
 * @method static Post create(array $data)
 */
class Post extends Model
{
    protected $fillable = ["title", "body", "excerpt", "category", "status", "author_id"];

    public function author()
    {
        return $this->belongsTo(User::class, 'author_id');
    }

    public function cat()
    {
        return $this->belongsTo(Category::class, 'category');
    }

    public function scopePublished(Builder $query)
    {
        return $query->where('status', 'PUBLISHED');
    }

    public function comments()
    {
        return $this->hasMany(Comment::class, 'post_id');
    }
}
