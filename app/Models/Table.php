<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/** @method table($table): Builder */
class Table extends Model
{
    protected $table = 'INFORMATION_SCHEMA.COLUMNS';

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('db', function (Builder $builder) {
            $builder->where('TABLE_SCHEMA', 'cluster');
        });

        static::addGlobalScope('column', function (Builder $builder) {
            $builder->select(['COLUMN_NAME', 'ORDINAL_POSITION', 'COLUMN_DEFAULT',
                'IS_NULLABLE', 'DATA_TYPE', 'CHARACTER_MAXIMUM_LENGTH',
                'NUMERIC_PRECISION', 'COLUMN_TYPE', 'COLUMN_KEY', 'EXTRA', 'TABLE_NAME']);
        });
    }

    public function scopeTable(Builder $builder, $table)
    {
        if ($table instanceof Model) {
            $table = $table->getTable();
        }

        $builder->where('TABLE_NAME', $table);
    }
}
