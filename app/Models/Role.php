<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed role
 */
class Role extends Model
{
    const ADMIN = "admin";
    const USER = "user";

    protected $primaryKey = 'role';
    protected $keyType = 'string';

    public function __toString()
    {
        return $this->role;
    }
}
