<?php

namespace App\Models\Forum;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * @property string channel
 * @method static Builder where(string $string, $channel)
 */
class Channel extends Model
{
    protected $primaryKey = 'channel';
    protected $keyType = 'string';

    public function __toString()
    {
        return $this->channel;
    }
}
