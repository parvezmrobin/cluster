<?php

namespace App\Models\Forum;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

/**
 * @property User user
 */
class Reply extends Model
{
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
