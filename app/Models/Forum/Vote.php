<?php

namespace App\Models\Forum;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int thread_id
 * @property int user_id
 * @property int is_upvote
 * @method static first(array $columns)
 * @method static create(array $columns)
 */
class Vote extends Model
{
    protected $fillable = ["user_id", "thread_id", "is_upvote"];
}
