<?php

namespace App\Models\Forum;

use App\Models\User;
use Conner\Tagging\Taggable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * @property Collection answers
 * @property int author_id
 * @property string title
 * @property string body
 * @property string channel
 * @property int id
 * @property string tags
 * @property User author
 * @method static create(array $data)
 * @method static findOrFail($id)
 * @method static published()
 */
class Thread extends Model
{
    use Taggable;

    protected $fillable = ["title", "body", "channel", "author_id"];

    public function author()
    {
        return $this->belongsTo(User::class, 'author_id');
    }

    public function answers()
    {
        return $this->hasMany(Answer::class);
    }

    public function cnl()
    {
        return $this->belongsTo(Channel::class, 'channel');
    }

    public function votes()
    {
        return $this->hasMany(Vote::class, 'thread_id');
    }

    public function upvotes()
    {
        return $this->votes()->where('is_upvote', 1);
    }

    public function downvotes()
    {
        return $this->votes()->where('is_upvote', 0);
    }

    public function scopePublished(Builder $query)
    {
        return $query->where('published', 1);
    }

    public function getTagsAttribute($tags)
    {
        return $tags;
    }

    public function getTagsArrayAttribute()
    {
        if (empty($this->tags)) {
            return collect();
        }
        return collect(explode(',', $this->tags));
    }
}
