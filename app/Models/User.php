<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as BaseUser;
use Illuminate\Notifications\Notifiable;
use Symfony\Component\Routing\Route;

/**
 * @property mixed name
 * @property mixed first_name
 * @property mixed last_name
 * @property int id
 * @property string role
 */
class User extends BaseUser
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password',
    ];

    protected $appends = ['name'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function user_role()
    {
        return $this->belongsTo(Role::class, 'role');
    }

    public function isAdmin()
    {
        return $this->role === Role::ADMIN;
    }

    public function getNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function __toString()
    {
        return $this->name;
    }
}
